syntax on
filetype on

set number
set expandtab
set autoindent
set softtabstop=4
set shiftwidth=4
set tabstop=4
set smarttab
set ruler
set noswapfile

set cursorline
set shortmess-=S
set title

"Enable mouse click for nvim
set mouse=a
"Fix cursor replacement after closing nvim
set guicursor=
set guicursor=n-v-c-sm:block,i-ci-ve:ver25-Cursor,r-cr-o:hor20

"Shift + Tab does inverse Tab
inoremap <S-Tab> <C-d>

" See invisible characters
set list listchars=tab:>\ ,trail:·,eol:↵,space:·
